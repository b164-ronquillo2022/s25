//example scenario


name: "jane",
enrollment: [
	
	{
		courseId: "1",
		status: "ongoing"
	},
	{
		courseId: "2",
		status: "completed"
	}

]


name: "Kelly",
enrollment: [
	
	{
		courseId: "3",
		status: "completed"
	},
	{
		courseId: "2",
		status: "drop"
	}

]


//MongoDB Aggregation
//Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data


//Using the aggregate method
//$match -> is used to pass the documents that meet the specified condition(1st stage)

//{$match: { field: value} }

db.fruits.aggregate([
    {$match: { onSale: true } }
])


//2nd stage
//$group -> is used to group elements together and field-value pairs using the data from the grouped elements

//{ $group: {_id: "value", fieldResult: "valueResult"} }

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", totalStocks: {$sum: "$stock"} } }
])


//the "$" symbol refer to a field name that is available in the documents that are being aggregated on
//$sum operator will total the values of all "stocks" fields in the returned documents that are found using the "$match" criteria



//Field Projection with aggregation

//$project can be used when aggregating data to include or exclude fields from the returned results
//{ $project: {field: 1/0} }
db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", totalStocks: {$sum: "$stock"} } },
    { $project: {_id: 0} }
])

//If we want to count all the documents

db.fruits.aggregate([
    { $group: {_id: null, myCount: {$sum: 1} } },
    { $project: {_id: 0} }
])


//If you specify an _id: value of null, or any other contant value, the $group stage calculate accumulated values for all the input documents as a whole.



//Sorting aggregated results
//$sort -> it can be used to change the order of aggregated results
//{$sort: {field: /1-1}}      -1 will sort the result in a reverse order

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", totalStocks: {$sum: "$stock"} } },
    { $sort: {totalStocks: 1} }
])


//Aggregating results based on array fields
//$unwind => deconstructs an array field from a collection/field with an array value to output a result for each element

//{ $unwind: field }

db.fruits.aggregate([
    { $unwind: "$origin" }
])

//The syntax will return results creating separate documents for each of the countries provided per the "origin" field



//displays fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
			{ $unwind : "$origin" },
			{ $group : { _id : "$origin" , kinds : { $sum : 1 } } }
		]);




//$count
db.scores.insertMany([
	{ "_id" : 1, "subject" : "History", "score" : 88 },
	{ "_id" : 2, "subject" : "History", "score" : 92 },
	{ "_id" : 3, "subject" : "History", "score" : 97 },
	{ "_id" : 4, "subject" : "History", "score" : 71 },
	{ "_id" : 5, "subject" : "History", "score" : 79 },
	{ "_id" : 6, "subject" : "History", "score" : 83 }
])


/*

The following aggregation operation has two stages:

1. The $match stage excludes documents that have a score value of less than or equal to 80 to pass along the documents with score greater than 80 to the next stage.
2. The $count stage returns a count of the remaining documents in the aggregation pipeline and assigns the value to a field called passing_scores.
*/


db.scores.aggregate([
    { $match: { score: { $gt: 80 } } },
    { $count: "passing_scores" }
])


/*

{ $count: <string> }
<string> is the name of the output field which has the count as its value. <string> must be a non-empty string, must not start with $ and must not contain the "." character.

*/
