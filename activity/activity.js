// Activity s25 Aggregation in MongoDB and Query Case Studies


 //2 fruits per country

       db.fruits.aggregate([
			{ $unwind : "$origin" },
			{ $group : { _id : "$origin" , fruits : { $sum : 1 } } }
		]);

//2 total number of fruits on sale
    	db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$count: "fruitsOnSale"}
		])
//3 total number of fruits with stock more than or equal to 20.

		db.fruits.aggregate([
			{$match:{"stock":{$gte:20}}},
			{$count: "enoughStock"}
		])

//4 get the average price of fruits onSale per supplier
		db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$group: {_id: "$supplier_id",
				"avg_price":{$avg: "price"}}}
		])

//5 get the highest price of a fruit per supplier.
		db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$group: {_id:"$supplier_id",
				max_price:{$max: "$price"}}}
		])

//6 min operator to get the lowest price of a fruit per supplier.
		db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$group: {_id:"$supplier_id",
				min_price:{$min: "$price"}}}
		])
